#include <iostream>
#include <string>

class Animal
{
protected:
    std::string m_name;
    Animal(std::string name) : m_name(name) {}

public:
    virtual std::string getClass() { return "Default Animal "; }
    std::string getName() { return m_name; }
    virtual std::string voice() { return "Default Voice!"; }
};

class Dog : public Animal
{
public:
    std::string getClass() override { return "Dog "; }
    Dog(std::string name) : Animal(name) {}
    std::string voice() override { return "Woof!"; }

    /*void voice() override
    {
        std::cout << "Woof!" << '\n';
    }*/
};

class Cat : public Animal
{
public:
    std::string getClass() override { return "Cat "; }
    Cat(std::string name) : Animal(name) {}
    std::string voice() override { return "Meow!"; }
};

class Bird : public Animal
{
public:
    std::string getClass() override { return "Bird "; }
    Bird(std::string name) : Animal(name) {}
    std::string voice() override { return "Tweet!"; }
};

int main()
{
    Cat max("Max"), tiger("Tiger"), smokey("Smokey");
    Dog milo("Milo"), buddy("Buddy"), duke("Duke");
    Bird woody("Woody"), tweety("Tweety"), rio("Rio");

    Animal* animals[] = { &max, &tiger, &smokey, &milo, &buddy, &duke, &woody, &tweety, &rio };
    for (int i = 0; i < 9; i++)
        std::cout << animals[i]->getClass() << animals[i]->getName() << " says " << animals[i]->voice() << '\n';

    return 0;

    /*Animal* ptr = new Dog("Puffy");
    std::cout << "Dog " << ptr->getName() << " says " << ptr->voice() << '\n';*/

    return 0;
}